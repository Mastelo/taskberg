@extends('layouts.app')

@section('content')

<div class="col-md-4 col-lg-4 col-md-offset-2  col-lg-offset-1">
    <div class="panel panel-primary ">
        <div class="panel-heading"> Mis Proyectos 
            <a  class="pull-right btn btn-primary btn-sm" 
                href="/projects/create">
                Create new </a> 
        </div>

        <div class="panel-body">
            
            <ul class="list-group">
            @foreach($projects as $project)
                <li class="list-group-item"> 
                <i class="fa fa-play" aria-hidden="true"></i>
                <a href="/projects/{{ $project->id }}" > {{ $project->name }}</a></li>
            @endforeach
            </ul>

        </div>
    </div>
</div>

<div class="col-md-12 col-lg-4   ">
        <div class="panel panel-primary ">
                <div class="panel-heading"> Invitado a </div>
            <ul class="list-group">
                    @foreach($proyectosInv as $proyectoInv)
                        <li class="list-group-item"> 
                        <i class="fa fa-play" aria-hidden="true"></i>
                        <a href="/projects/{{ $proyectoInv->id }}" > {{ $proyectoInv->name }}</a></li>
                    @endforeach
            </ul>
        </div>
</div>

@endsection